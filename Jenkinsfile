pipeline {
  agent none
  parameters {
        choice(
                choices: 'dev1\ndev2\ndev3\ndev4\ndev5\ndev6\ndev7\ndev8\ndev9\nintegration\nregression\nstaging',
                description: 'What test environment create?',
                name: 'envName'
              )
  }

  environment {
    APP = 'secrets'
    REGISTRY = 'git.matchesremote.com:4567'
    IMAGE_NAME = 'git.matchesremote.com:4567/kubernetes/secrets'
  }

  stages {
    stage('Dry Run') {
      agent any

      steps {
        checkout scm
        withEnv(["PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:"]) {
          sh "helm install --dry-run --debug -f helm/values.yaml -f helm/values-${params.envName}.yaml --name ${APP} ./helm"
         }

        input "apply changes?"
      }
    }

    stage('Deployment') {
      agent any

      when {
        expression { env.BRANCH_NAME == 'master' }
      }

      environment {
        RELEASE_NAME = "${params.envName}-${APP}"
      }

      steps {
        withEnv(["PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:"]) {
          sh "kubectl get ns ${params.envName} || kubectl create ns ${params.envName}"
          sh "helm upgrade --install --wait --namespace ${params.envName} -f helm/values.yaml -f helm/values-${params.envName}.yaml ${RELEASE_NAME} ./helm"
        }
      }
    }
  }
}
